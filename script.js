let app,
  player,
  playerCloud,
  scoreText,
  goldText,
  loadingText,
  finalScore,
  titleScreen,
  goldTextShop,
  gameScreen,
  pauseScreen,
  gameOverScreen,
  shopScreen,
  background,
  clouds,
  unlock1,
  unlock2,
  explosionSound,
  bulletSound,
  gameOverSound,
  gameSound,
  coinSound,
  errorSound,
  coinAlert,
  alertBox,
  akanzaLogo;
let bullets = [];
let enemies = [];
let lootBoxes = [];
let gameOver = false;
let gamePaused = false;
let first = true;
let score = 0;
let gold = 0;
let totalScore = 0;
let backgroundY = 0;
let bulletPower = 0;
let difficulity = 1;
let enemiesSpeed = 1;
let upgradeLevel = 0;
const blackAndWhite = new PIXI.filters.ColorMatrixFilter();
blackAndWhite.blackAndWhite(true);

window.onload = () => {
  startNewGame();
};

const isMobileDevice = () => {
  return (
    typeof window.orientation !== 'undefined' ||
    navigator.userAgent.indexOf('IEMobile') !== -1
  );
};

const startNewGame = () => {
  PIXI.settings.PRECISION_FRAGMENT = PIXI.PRECISION.HIGH;
  PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.LINEAR;
  if (isMobileDevice()) {
    app = new PIXI.Application({
      width: window.innerWidth,
      height: window.innerHeight,
      backgroundColor: 0xffffff,
      autoDensity: true,
      autoResize: true,
    });
  } else {
    app = new PIXI.Application({
      width: 345,
      height: 600,
      backgroundColor: 0xffffff,
      autoResize: true,
    });
  }

  akanzaLogo = new PIXI.Sprite.fromImage('assets/akanza/logo.svg');
  akanzaLogo.anchor.set(0.5);
  akanzaLogo.x = app.view.width / 2;
  akanzaLogo.y = app.view.height / 3;
  app.stage.addChild(akanzaLogo);

  loadingText = new PIXI.Text('0%');
  loadingText.style = new PIXI.TextStyle({
    fill: 0x993300,
    fontFamily: 'LuckiestGuy',
    stroke: 0xffff99,
    strokeThickness: 5,
    fontSize: 48,
  });
  loadingText.anchor.set(0.5);
  loadingText.x = app.view.width / 2;
  loadingText.y = app.view.height / 2;
  app.stage.addChild(loadingText);

  document.getElementById('game').appendChild(app.view);

  setupSoundEffects();
  preloadAssets();
};

const preloadAssets = () => {
  app.loader.baseUrl = 'assets';
  app.loader
    .add('player01', 'Plane/01/Plane1Up.svg')
    .add('player01up1', 'Plane/01/Plane2Up.svg')
    .add('player01up2', 'Plane/01/Plane3Up.svg')
    .add('bullet01', 'Bullets/01Bullets.svg')
    .add('bullet02', 'Bullets/02Bullets.svg')
    .add('bullet03', 'Bullets/03Bullets.svg')
    .add('bullet04', 'Bullets/04Bullets.svg')
    .add('bullet05', 'Bullets/05Bullets.svg')
    .add('bullet06', 'Bullets/06Bullets.svg')
    .add('bullet07', 'Bullets/07Bullets.svg')
    .add('bullet08', 'Bullets/08Bullets.svg')
    .add('bullet09', 'Bullets/09Bullets.svg')
    .add('enemy01', 'Enemy/Enemy02.svg')
    .add('enemy02', 'Enemy/Enemy03.svg')
    .add('enemy03', 'Enemy/Enemy04.svg')
    .add('enemy04', 'Enemy/Enemy06.svg')
    .add('enemy05', 'Enemy/Enemy07.svg')
    .add('enemy06', 'Enemy/Enemy10.svg')
    .add('enemy07', 'Enemy/Enemy11.svg')
    .add('boss01', 'Enemy/Enemy01.svg')
    .add('boss02', 'Enemy/Enemy05.svg')
    .add('boss03', 'Enemy/Enemy08.svg')
    .add('boss04', 'Enemy/Enemy13.svg')
    .add('boss05', 'Enemy/Enemy14.svg')
    .add('background01', 'Backgrounds/01Background.svg')
    .add('background02', 'Backgrounds/02Background.svg')
    .add('background03', 'Backgrounds/03Background.svg')
    .add('background04', 'Backgrounds/04Background.svg')
    .add('background05', 'Backgrounds/05Background.svg')
    .add('clouds', 'Backgrounds/WhiteCloud.svg')
    .add('explosion01', 'Items/ExplosionSprite/01.svg')
    .add('explosion02', 'Items/ExplosionSprite/02.svg')
    .add('explosion03', 'Items/ExplosionSprite/03.svg')
    .add('explosion04', 'Items/ExplosionSprite/04.svg')
    .add('planeCloud01', 'Items/PlanCloudGas/PlaneCloudGas01.svg')
    .add('planeCloud02', 'Items/PlanCloudGas/PlaneCloudGas02.svg')
    .add('planeCloud03', 'Items/PlanCloudGas/PlaneCloudGas03.svg')
    .add('planeCloud04', 'Items/PlanCloudGas/PlaneCloudGas04.svg')
    .add('gold01', 'Items/BoxItems/CoinBoxSprite01.svg')
    .add('gold02', 'Items/BoxItems/CoinBoxSprite02.svg')
    .add('titleBackground', 'UserInterface/Menu/Background.svg')
    .add('startButton', 'UserInterface/AirportPanelScreen/startFlyBtn.png')
    .add('pauseBackground', 'UserInterface/AirportPanelScreen/Background.svg')
    .add('missionFailed', 'UserInterface/failConditionsBox/imageBg.png')
    .add('restartButton', 'UserInterface/failConditionsBox/RestartBtn.png')
    .add('menuButton', 'UserInterface/failConditionsBox/MenuBtn.png')
    .add('closeButton', 'UserInterface/PausedBox/CloseBtn.svg')
    .add('upgradeButton', 'UserInterface/AirportPanelScreen/UpgBtn.png')
    .add('unlockButton', 'UserInterface/UnlockBox/UnlockBtn.png')
    .add('coinAlert', 'UserInterface/CoinReqBox/text.png')
    .add('alertBox', 'UserInterface/CoinReqBox/BoxBg.svg')
    .add('rat', 'Enemy/boss.png')
    .add('akanza', 'akanza/by_akanza.svg');

  app.loader.onProgress.add(showProgress);
  app.loader.onComplete.add(doneLoading);
  app.loader.onError.add(reportError);

  app.loader.load();
};

const showProgress = e => {
  console.log(Math.round(e.progress));
  loadingText.text = `${Math.round(e.progress)}%`;
};

const reportError = e => {
  console.error(e.message);
};

const doneLoading = () => {
  console.log('Done loading');
  app.stage.removeChild(loadingText);
  app.stage.removeChild(akanzaLogo);
  createScreens();
};

const createScreens = () => {
  titleScreen = new PIXI.Container();
  titleScreen.visible = true;
  app.stage.addChild(titleScreen);
  gameScreen = new PIXI.Container();
  gameScreen.visible = false;
  app.stage.addChild(gameScreen);
  pauseScreen = new PIXI.Container();
  pauseScreen.visible = false;
  app.stage.addChild(pauseScreen);
  gameOverScreen = new PIXI.Container();
  gameOverScreen.visible = false;
  app.stage.addChild(gameOverScreen);
  shopScreen = new PIXI.Container();
  shopScreen.visible = false;
  app.stage.addChild(shopScreen);
  setupTitleScreen();
  setupGameScreen();
  setupPauseScreen();
  setupGameOverScreen();
  setupShopScreen();
};

const setupTitleScreen = () => {
  const titleBackground = new PIXI.Sprite.from(
    app.loader.resources.titleBackground.texture
  );
  titleBackground.width = app.view.width;
  titleBackground.height = app.view.height;
  titleScreen.addChild(titleBackground);
  const startButton = new PIXI.Sprite.from(
    app.loader.resources.startButton.texture
  );
  startButton.anchor.set(0.5);
  startButton.x = app.view.width / 2;
  startButton.y = app.view.height / 2 - 40;
  startButton.interactive = true;
  startButton.buttonMode = true;
  startButton.on('click', startGame);
  startButton.on('tap', startGame);
  titleScreen.addChild(startButton);
  const upgradeButton = new PIXI.Sprite.from(
    app.loader.resources.upgradeButton.texture
  );
  upgradeButton.anchor.set(0.5);
  upgradeButton.x = app.view.width / 2;
  upgradeButton.y = startButton.y + 60;
  upgradeButton.interactive = true;
  upgradeButton.buttonMode = true;
  upgradeButton.on('click', upgradePlane);
  upgradeButton.on('tap', upgradePlane);
  titleScreen.addChild(upgradeButton);
  const akanza = new PIXI.Sprite.from(app.loader.resources.akanza.texture);
  akanza.x = app.view.width - akanza.width;
  akanza.y = app.view.height - akanza.height;
  titleScreen.addChild(akanza);
};

const setupGameScreen = () => {
  gameScreen.cursor = 'none';
  background = createBackground(
    app.loader.resources[generateBackground()].texture
  );
  background.visible = false;
  clouds = createBackground(app.loader.resources.clouds.texture);
  clouds.visible = false;
  addGamePause();
  createPlayer();
  createPlayerCloud();
  createScoreText();
};

const setupPauseScreen = () => {
  pauseScreen.cursor = 'pointer';
  const pausedBackground = new PIXI.Sprite.from(
    app.loader.resources.pauseBackground.texture
  );
  pausedBackground.width = app.view.width;
  pausedBackground.height = app.view.height;
  pauseScreen.addChild(pausedBackground);
  const pauseText = new PIXI.Text('Paused');
  pauseText.style = new PIXI.TextStyle({
    fill: 0x993300,
    fontFamily: 'LuckiestGuy',
    stroke: 0xffff99,
    strokeThickness: 5,
    fontSize: 48,
  });
  pauseText.anchor.set(0.5);
  pauseText.x = app.view.width / 2;
  pauseText.y = app.view.height / 3;
  pauseScreen.addChild(pauseText);
  const restartButton = new PIXI.Sprite.from(
    app.loader.resources.restartButton.texture
  );
  restartButton.anchor.set(0.5);
  restartButton.x = app.view.width / 2;
  restartButton.y = pauseText.y + 120;
  restartButton.interactive = true;
  restartButton.buttonMode = true;
  restartButton.on('click', restartGame);
  restartButton.on('tap', restartGame);
  pauseScreen.addChild(restartButton);
  const closeButton = new PIXI.Sprite.from(
    app.loader.resources.closeButton.texture
  );
  closeButton.x = app.view.width - closeButton.width - 10;
  closeButton.y = 10;
  closeButton.interactive = true;
  closeButton.buttonMode = true;
  closeButton.on('click', resumeGame);
  closeButton.on('tap', resumeGame);
  pauseScreen.addChild(closeButton);
};

const setupGameOverScreen = () => {
  gameOverScreen.cursor = 'pointer';
  const gameOverBackground = new PIXI.Sprite.from(
    app.loader.resources.pauseBackground.texture
  );
  gameOverBackground.width = app.view.width;
  gameOverBackground.height = app.view.height;
  gameOverScreen.addChild(gameOverBackground);
  const missionFailed = new PIXI.Sprite.from(
    app.loader.resources.missionFailed.texture
  );
  missionFailed.anchor.set(0.5);
  missionFailed.x = app.view.width / 2;
  missionFailed.y = app.view.height / 3;
  gameOverScreen.addChild(missionFailed);
  const restartButton = new PIXI.Sprite.from(
    app.loader.resources.restartButton.texture
  );
  restartButton.anchor.set(0.5);
  restartButton.x = app.view.width / 2;
  restartButton.y = missionFailed.y + 240;
  restartButton.interactive = true;
  restartButton.buttonMode = true;
  restartButton.on('click', restartGame);
  restartButton.on('tap', restartGame);
  gameOverScreen.addChild(restartButton);
  const menuButton = new PIXI.Sprite.from(
    app.loader.resources.menuButton.texture
  );
  menuButton.anchor.set(0.5);
  menuButton.x = app.view.width / 2;
  menuButton.y = restartButton.y + 60;
  menuButton.interactive = true;
  menuButton.buttonMode = true;
  menuButton.on('click', showMenu);
  menuButton.on('tap', showMenu);
  gameOverScreen.addChild(menuButton);
};

const setupShopScreen = () => {
  shopScreen.cursor = 'pointer';
  const shopScreenBackground = new PIXI.Sprite.from(
    app.loader.resources.pauseBackground.texture
  );
  shopScreenBackground.width = app.view.width;
  shopScreenBackground.height = app.view.height;
  shopScreen.addChild(shopScreenBackground);
  const upgradeText = new PIXI.Text('UPGRADE');
  upgradeText.style = new PIXI.TextStyle({
    fill: 0x993300,
    fontFamily: 'LuckiestGuy',
    stroke: 0xffff99,
    strokeThickness: 5,
    fontSize: 48,
  });
  upgradeText.anchor.set(0.5);
  upgradeText.x = app.view.width / 2;
  upgradeText.y = app.view.height / 3 - 60;
  shopScreen.addChild(upgradeText);
  goldTextShop = new PIXI.Text('Gold: ' + gold);
  goldTextShop.style = new PIXI.TextStyle({
    fill: 0xffff99,
    fontFamily: 'LuckiestGuy',
    stroke: 0x993300,
    strokeThickness: 5,
    fontSize: 30,
  });
  goldTextShop.anchor.set(0.5);
  goldTextShop.x = app.view.width / 2;
  goldTextShop.y = upgradeText.y + 60;
  shopScreen.addChild(goldTextShop);
  const upgrade1 = new PIXI.Sprite.from(
    app.loader.resources.player01up1.texture
  );
  upgrade1.anchor.set(0.5);
  upgrade1.x = app.view.width / 3 - 20;
  upgrade1.y = goldTextShop.y + 120;
  shopScreen.addChild(upgrade1);
  unlock1 = new PIXI.Sprite.from(app.loader.resources.unlockButton.texture);
  unlock1.anchor.set(0.5);
  unlock1.x = (2 * app.view.width) / 3 + 20;
  unlock1.y = upgrade1.y - 20;
  unlock1.interactive = true;
  unlock1.buttonMode = true;
  unlock1.on('click', buyUpgrade);
  unlock1.on('tap', buyUpgrade);
  shopScreen.addChild(unlock1);
  const price1 = new PIXI.Text('100 gold');
  price1.style = new PIXI.TextStyle({
    fill: 0xffff99,
    fontFamily: 'LuckiestGuy',
    stroke: 0x993300,
    strokeThickness: 3,
    fontSize: 16,
  });
  price1.anchor.set(0.5);
  price1.x = unlock1.x;
  price1.y = unlock1.y + 40;
  shopScreen.addChild(price1);
  const upgrade2 = new PIXI.Sprite.from(
    app.loader.resources.player01up2.texture
  );
  upgrade2.anchor.set(0.5);
  upgrade2.x = app.view.width / 3 - 20;
  upgrade2.y = upgrade1.y + 150;
  shopScreen.addChild(upgrade2);
  unlock2 = new PIXI.Sprite.from(app.loader.resources.unlockButton.texture);
  unlock2.anchor.set(0.5);
  unlock2.x = (2 * app.view.width) / 3 + 20;
  unlock2.y = upgrade2.y - 20;
  unlock2.interactive = true;
  unlock2.buttonMode = true;
  unlock2.on('click', buySecondUpgrade);
  unlock2.on('tap', buySecondUpgrade);
  shopScreen.addChild(unlock2);
  const price2 = new PIXI.Text('1000 gold');
  price2.style = new PIXI.TextStyle({
    fill: 0xffff99,
    fontFamily: 'LuckiestGuy',
    stroke: 0x993300,
    strokeThickness: 3,
    fontSize: 16,
  });
  price2.anchor.set(0.5);
  price2.x = unlock2.x;
  price2.y = unlock2.y + 40;
  shopScreen.addChild(price2);
  const closeButton = new PIXI.Sprite.from(
    app.loader.resources.closeButton.texture
  );
  closeButton.x = app.view.width - closeButton.width - 10;
  closeButton.y = 10;
  closeButton.interactive = true;
  closeButton.buttonMode = true;
  closeButton.on('click', showMenu);
  closeButton.on('tap', showMenu);
  shopScreen.addChild(closeButton);
  alertBox = new PIXI.Sprite.from(app.loader.resources.alertBox.texture);
  alertBox.anchor.set(0.5);
  alertBox.x = app.view.width / 2;
  alertBox.y = 60;
  alertBox.width = 200;
  alertBox.height = 120;
  alertBox.visible = false;
  shopScreen.addChild(alertBox);
  coinAlert = new PIXI.Sprite.from(app.loader.resources.coinAlert.texture);
  coinAlert.anchor.set(0.5);
  coinAlert.x = app.view.width / 2;
  coinAlert.y = 60;
  coinAlert.visible = false;
  shopScreen.addChild(coinAlert);
};

const setupSoundEffects = () => {
  explosionSound = new Howl({
    src: ['assets/sounds/explosion.wav'],
    preload: true,
  });
  bulletSound = new Howl({
    src: ['assets/sounds/bullet.wav'],
    preload: true,
  });
  gameOverSound = new Howl({
    src: ['assets/sounds/gameOver.wav'],
    preload: true,
  });
  gameSound = new Howl({
    src: ['assets/sounds/gameSound.wav'],
    preload: true,
    loop: true,
  });
  coinSound = new Howl({
    src: ['assets/sounds/coin.wav'],
    preload: true,
  });
  errorSound = new Howl({
    src: ['assets/sounds/error.wav'],
    preload: true,
  });
};

const upgradePlane = () => {
  goldTextShop.text = `Gold: ${gold}`;
  titleScreen.visible = false;
  shopScreen.visible = true;
  background.visible = false;
  clouds.visible = false;
};

const toggleCoinAlert = () => {
  alertBox.visible = !alertBox.visible;
  coinAlert.visible = !coinAlert.visible;
};

const buyUpgrade = () => {
  if (gold >= 100 && upgradeLevel < 1) {
    gold -= 100;
    upgradeLevel = 1;
    unlock1.interactive = false;
    unlock1.filters = [blackAndWhite];
    player.texture = app.loader.resources.player01up1.texture;
    goldTextShop.text = `Gold: ${gold}`;
  } else {
    errorSound.play();
    toggleCoinAlert();
    setTimeout(() => {
      toggleCoinAlert();
    }, 1500);
  }
};

const buySecondUpgrade = () => {
  if (gold >= 1000 && upgradeLevel < 2) {
    gold -= 1000;
    upgradeLevel = 2;
    unlock2.interactive = false;
    unlock1.filters = [blackAndWhite];
    unlock2.filters = [blackAndWhite];
    player.texture = app.loader.resources.player01up2.texture;
    goldTextShop.text = `Gold: ${gold}`;
  } else {
    errorSound.play();
    toggleCoinAlert();
    setTimeout(() => {
      toggleCoinAlert();
    }, 1500);
  }
};

const generateBackground = () => {
  const backgrounds = [
    'background01',
    'background02',
    'background03',
    'background04',
    'background05',
  ];
  const index = Math.floor(Math.random() * 5);
  return backgrounds[index];
};

const startGame = () => {
  console.log('game started');
  gameSound.play();
  titleScreen.visible = false;
  pauseScreen.visible = false;
  gameOverScreen.visible = false;
  background.visible = true;
  clouds.visible = true;
  gameScreen.visible = true;
  score = 0;
  if (first) {
    app.ticker.add(gameLoop);
    setInterval(spawnEnemy, 1000);
    setInterval(spawnBoss, 10000);
    first = false;
  } else {
    restartGame();
  }
};

const showMenu = () => {
  gameOverScreen.removeChild(goldText);
  gameOverScreen.removeChild(finalScore);
  gameOver = false;
  gameOverScreen.visible = false;
  titleScreen.visible = true;
  shopScreen.visible = false;
  background.visible = false;
  clouds.visible = false;
};

const restartGame = () => {
  gameSound.play();
  score = 0;
  bulletPower = 0;
  difficulity = 1;
  enemiesSpeed = 1;
  setScoreText(0);
  gameOverScreen.removeChild(goldText);
  gameOverScreen.removeChild(finalScore);
  bullets.forEach(bullet => gameScreen.removeChild(bullet));
  bullets = [];
  enemies.forEach(enemy => gameScreen.removeChild(enemy));
  enemies = [];
  lootBoxes.forEach(lootBox => gameScreen.removeChild(lootBox));
  lootBoxes = [];
  player.x = app.view.width / 2;
  player.y = app.view.height / 2;
  playerCloud.x = player.x;
  playerCloud.y = player.y + 30;
  background.texture = app.loader.resources[generateBackground()].texture;
  gameOver = false;
  gamePaused = false;
  pauseScreen.visible = false;
  gameOverScreen.visible = false;
  background.visible = true;
  clouds.visible = true;
  gameScreen.visible = true;
};

const resumeGame = () => {
  gamePaused = false;
  pauseScreen.visible = false;
  background.visible = true;
  clouds.visible = true;
  gameScreen.visible = true;
};

const addGamePause = () => {
  window.addEventListener('keydown', pauseGame);
};

const pauseGame = e => {
  if (e.keyCode === 27) {
    gamePaused = !gamePaused;
    if (gamePaused) {
      gameScreen.visible = false;
      background.visible = false;
      clouds.visible = false;
      pauseScreen.visible = true;
    } else {
      pauseScreen.visible = false;
      background.visible = true;
      clouds.visible = true;
      gameScreen.visible = true;
    }
  }
};

const createBackground = texture => {
  let tiling;
  if (isMobileDevice()) {
    tiling = new PIXI.TilingSprite(
      texture,
      window.innerWidth,
      window.innerHeight
    );
    tiling.scale.x = 1.3;
    tiling.scale.y = 1.3;
  } else {
    tiling = new PIXI.TilingSprite(texture, 345, 600);
    tiling.scale.x = 1.2;
    tiling.scale.y = 1.2;
  }
  tiling.position.set(0, 0);
  gameScreen.addChild(tiling);
  return tiling;
};

const createScoreText = () => {
  scoreText = new PIXI.Text('Score: 0');
  scoreText.style = new PIXI.TextStyle({
    fill: 0x993300,
    fontFamily: 'LuckiestGuy',
    stroke: 0xffff99,
    strokeThickness: 5,
  });
  scoreText.zIndex = 5;
  scoreText.x = app.view.width - scoreText.width - 10;
  scoreText.y = 10;
  gameScreen.addChild(scoreText);
};

const setScoreText = value => {
  score += value;
  scoreText.text = 'Score: ' + score;
  scoreText.updateText();
  scoreText.x = app.view.width - scoreText.width - 10;
};

const createPlayer = () => {
  player = new PIXI.Sprite.from(app.loader.resources.player01.texture);
  player.anchor.set(0.5);
  player.x = app.view.width / 2;
  player.y = app.view.height / 2;
  player.scale.x = 0.5;
  player.scale.y = 0.5;
  gameScreen.addChild(player);
  setPlayerMovement();
};

const createPlayerCloud = () => {
  const playerClouds = [
    'planeCloud01',
    'planeCloud02',
    'planeCloud03',
    'planeCloud04',
  ];
  const textures = [];
  playerClouds.forEach(item => {
    textures.push(app.loader.resources[item].texture);
  });
  playerCloud = new PIXI.AnimatedSprite(textures);
  playerCloud.anchor.set(0.5);
  playerCloud.scale.x = 0.5;
  playerCloud.scale.y = 0.5;
  playerCloud.x = player.x;
  playerCloud.y = player.y + 30;
  playerCloud.animationSpeed = 0.2;
  gameScreen.addChild(playerCloud);
  playerCloud.play();
};

const setPlayerMovement = () => {
  gameScreen.interactive = true;
  gameScreen.on('pointermove', movePlayer);
  gameScreen.on('pointerdown', fireBullet);
};

const movePlayer = e => {
  const pos = e.data.global;
  player.x = pos.x;
  player.y = pos.y;
  playerCloud.x = pos.x;
  playerCloud.y = pos.y + 30;
};

const createEnemy = () => {
  const enemies = [
    'enemy01',
    'enemy02',
    'enemy03',
    'enemy04',
    'enemy05',
    'enemy06',
    'enemy07',
  ];
  const enemiesHealth = [4, 3, 3, 2, 2, 1, 1];
  const index = Math.floor(Math.random() * 7);
  let enemy = new PIXI.Sprite.from(
    app.loader.resources[enemies[index]].texture
  );
  enemy.anchor.set(0.5);
  enemy.x = generateEnemyX();
  enemy.y = 0;
  enemy.speed = index + 2 + enemiesSpeed;
  enemy.scale.x = 0.5;
  enemy.scale.y = 0.5;
  enemy.health = enemiesHealth[index] * difficulity;
  gameScreen.addChild(enemy);
  return enemy;
};

const generateEnemyX = () => {
  let x = Math.floor(Math.random() * app.view.width);
  if (x < 62) {
    x += 62;
  } else if (x > app.view.width - 62) {
    x -= 62;
  }
  return x;
};

const spawnEnemy = () => {
  if (!gamePaused) {
    const enemy = createEnemy();
    enemies.push(enemy);
  }
};

const createBoss = () => {
  //const bosses = ['boss01', 'boss02', 'boss03', 'boss04', 'boss05'];
  //const index = Math.floor(Math.random() * 5);
  //let boss = new PIXI.Sprite.from(app.loader.resources[bosses[index]].texture);
  let boss = new PIXI.Sprite.from(app.loader.resources.rat.texture);
  boss.anchor.set(0.5);
  boss.x = app.view.width / 2;
  boss.y = 0;
  boss.speed = 1;
  boss.health = 10 * difficulity;
  gameScreen.addChild(boss);
  return boss;
};

const spawnBoss = () => {
  if (!gamePaused) {
    const boss = createBoss();
    enemies.push(boss);
  }
};

const updateEnemyPosition = () => {
  enemies.forEach((enemy, index) => {
    enemy.position.y += enemy.speed;
    if (enemy.position.y > app.view.height) {
      gameScreen.removeChild(enemy);
      enemies.splice(index, 1);
      gameOver = true;
    }
  });
};

const createBullet = () => {
  const bullets = [
    'bullet01',
    'bullet02',
    'bullet03',
    'bullet04',
    'bullet05',
    'bullet06',
    'bullet07',
    'bullet08',
    'bullet09',
  ];
  let bullet = new PIXI.Sprite.from(
    app.loader.resources[bullets[bulletPower]].texture
  );
  bullet.anchor.set(0.5);
  bullet.x = player.x;
  bullet.y = player.y - 30;
  bullet.speed = 10;
  bullet.power = bulletPower + upgradeLevel + 1;
  gameScreen.addChild(bullet);
  return bullet;
};

const fireBullet = () => {
  const bullet = createBullet();
  bulletSound.play();
  bullets.push(bullet);
};

const updateBulletsPosition = () => {
  bullets.forEach((bullet, index) => {
    bullet.position.y -= bullet.speed;
    if (bullet.position.y < 0) {
      gameScreen.removeChild(bullet);
      bullets.splice(index, 1);
    }
  });
};

const checkCollision = (a, b) => {
  const aBox = a.getBounds();
  const bBox = b.getBounds();

  return (
    aBox.x + aBox.width - 62 > bBox.x &&
    aBox.x < bBox.x + bBox.width - 62 &&
    aBox.y + aBox.height - 95 > bBox.y &&
    aBox.y < bBox.y + bBox.height - 95
  );
};

const createExplosion = (x, y) => {
  const explosions = [
    'explosion01',
    'explosion02',
    'explosion03',
    'explosion04',
  ];
  const textures = [];
  explosions.forEach(item => {
    textures.push(app.loader.resources[item].texture);
  });
  const explosion = new PIXI.AnimatedSprite(textures);
  explosion.anchor.set(0.5);
  explosion.x = x;
  explosion.y = y;
  explosion.loop = false;
  explosion.animationSpeed = 0.2;
  explosion.onComplete = () => {
    explosion.stop();
    gameScreen.removeChild(explosion);
  };
  gameScreen.addChild(explosion);
  explosion.play();
  return explosion;
};

const createLoot = (x, y) => {
  const random = Math.floor(Math.random() * 100);
  if (random <= 30) {
    const lootBox = new PIXI.AnimatedSprite([
      app.loader.resources.gold01.texture,
      app.loader.resources.gold02.texture,
    ]);
    lootBox.anchor.set(0.5);
    lootBox.scale.x = 0.5;
    lootBox.scale.y = 0.5;
    lootBox.x = x;
    lootBox.y = y;
    lootBox.animationSpeed = 0.1;
    lootBox.gold = Math.floor(Math.random() * 50);
    gameScreen.addChild(lootBox);
    lootBox.play();
    lootBoxes.push(lootBox);
  }
};

const updateLootPosition = () => {
  lootBoxes.forEach((lootBox, index) => {
    lootBox.position.y += 5;
    if (lootBox.position.y > app.view.height) {
      gameScreen.removeChild(lootBox);
      lootBoxes.splice(index, 1);
    }
  });
};

const checkPlayerLootBoxCollision = () => {
  lootBoxes.forEach((lootBox, index) => {
    if (checkCollision(player, lootBox)) {
      gold += lootBox.gold;
      coinSound.play();
      gameScreen.removeChild(lootBox);
      lootBoxes.splice(index, 1);
    }
  });
};

const checkPlayerEnemiesCollision = () => {
  enemies.forEach(enemy => {
    if (checkCollision(player, enemy)) {
      gameOver = true;
    }
  });
};

const checkBulletsEnemiesCollision = () => {
  bullets.forEach((bullet, bulletIndex) => {
    enemies.forEach((enemy, enemyIndex) => {
      if (checkCollision(enemy, bullet)) {
        bullet.visible = false;
        gameScreen.removeChild(bullet);
        bullets.splice(bulletIndex, 1);
        enemy.health -= bullet.power + 1;
        if (enemy.health <= 0) {
          createExplosion(enemy.x, enemy.y);
          explosionSound.play();
          createLoot(enemy.x, enemy.y);
          gameScreen.removeChild(enemy);
          enemies.splice(enemyIndex, 1);
          setScoreText(1);
        }
      }
    });
  });
};

const setFinalScore = () => {
  finalScore = new PIXI.Text('Score: ' + totalScore);
  finalScore.style = new PIXI.TextStyle({
    fill: 0x993300,
    fontFamily: 'LuckiestGuy',
    stroke: 0xffff99,
    strokeThickness: 5,
    fontSize: 30,
  });
  finalScore.anchor.set(0.5);
  finalScore.x = app.view.width / 2;
  finalScore.y = app.view.height / 2 + 20;
  gameOverScreen.addChild(finalScore);
  goldText = new PIXI.Text('Gold: ' + gold);
  goldText.style = new PIXI.TextStyle({
    fill: 0xffff99,
    fontFamily: 'LuckiestGuy',
    stroke: 0x993300,
    strokeThickness: 5,
    fontSize: 30,
  });
  goldText.anchor.set(0.5);
  goldText.x = app.view.width / 2;
  goldText.y = 375;
  gameOverScreen.addChild(goldText);
};

const moveBackground = () => {
  backgroundY = backgroundY + 2;
  background.tilePosition.y = backgroundY / 2;
  clouds.tilePosition.y = backgroundY;
};

const checkScore = () => {
  if (score >= 10) {
    bulletPower = 1;
    difficulity = 2;
  }
  if (score >= 20) {
    bulletPower = 2;
    difficulity = 3;
    enemiesSpeed = 2;
  }
  if (score >= 30) {
    bulletPower = 3;
    difficulity = 4;
  }
  if (score >= 40) {
    bulletPower = 4;
    difficulity = 5;
    enemiesSpeed = 3;
  }
  if (score >= 50) {
    bulletPower = 5;
    difficulity = 6;
  }
  if (score >= 60) {
    bulletPower = 6;
    difficulty = 7;
    enemiesSpeed = 4;
  }
  if (score >= 70) {
    bulletPower = 7;
    difficulity = 8;
  }
  if (score >= 80) {
    bulletPower = 8;
    difficulity = 9;
    enemiesSpeed = 5;
  }
};

const gameLoop = () => {
  if (gameScreen.visible) {
    checkScore();
    moveBackground();
    updateBulletsPosition();
    updateEnemyPosition();
    updateLootPosition();
    checkPlayerEnemiesCollision();
    checkBulletsEnemiesCollision();
    checkPlayerLootBoxCollision();
  }
  if (gameOver) {
    gameSound.stop();
    bulletSound.stop();
    explosionSound.stop();
    gameOverSound.play();
    totalScore = score;
    score = 0;
    bulletPower = 0;
    difficulity = 1;
    enemiesSpeed = 1;
    setScoreText(0);
    setFinalScore();
    gameOver = false;
    gameScreen.visible = false;
    background.visible = false;
    clouds.visible = false;
    gameOverScreen.visible = true;
  }
};
